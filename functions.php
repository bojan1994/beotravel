<?php

/* Register scripts */
if( ! function_exists( 'register_scripts' ) ) {
    function register_scripts() {
        wp_enqueue_style( 'fontawesome.min', get_template_directory_uri() . '/assets/css/fontawesome.min.css' );
        wp_enqueue_style( 'fontawesome-all.min', get_template_directory_uri() . '/assets/css/fontawesome-all.min.css' );
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
        wp_enqueue_style( 'style', get_stylesheet_uri(), array( 'bootstrap' ) );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array( 'bootstrap' ) );
    }
    add_action( 'wp_enqueue_scripts', 'register_scripts' );
}

/* Register Navigation Menus */
register_nav_menus( array(
    'primary' => __( 'Primary Navigation' ),
    'footer' => __( 'Footer Navigation' ),
    'social' => __( 'Social networks' ),
    'languages' => __( 'Languages' ),
) );

/* Theme Setup */
if( ! function_exists( 'theme_setup' ) ) {
    function theme_setup() {
        $array = array(
            'height'      => 70,
            'width'       => 230,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $array );
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'our-team', 110, 165 );
        add_theme_support( 'title-tag' );
    }
    add_action( 'after_setup_theme', 'theme_setup' );
}

/* Custom footer */
if( ! function_exists( 'theme_customizer' ) ) {
    function theme_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_footer_text', array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text', array(
            'title' => __( 'Custom footer text', 'beotravel' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom_footer', array(
            'label' => __( 'Footer Text', 'beotravel' ),
            'section' => 'footer_text',
            'settings' => 'custom_footer_text',
            'type' => 'textarea'
        ) ) );
    }
    add_action( 'customize_register', 'theme_customizer' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type' ) ) {
    function slider_post_type() {
        $labels = array(
            'name' => __( 'Slider', 'beotravel' ),
            'singular_name' => __( 'Slider', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider',
            ),
        );
        register_post_type( 'slider', $args );
    }
    add_action( 'init','slider_post_type' );
}

/* Our team post type */
if( ! function_exists( 'ourteam_post_type' ) ) {
    function ourteam_post_type() {
        $labels = array(
            'name' => __( 'Our team', 'beotravel' ),
            'singular_name' => __( 'Our team', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'ourteam',
            ),
        );
        register_post_type( 'ourteam', $args );
    }
    add_action( 'init','ourteam_post_type' );
}

/* References post type */
if( ! function_exists( 'references_post_type' ) ) {
    function references_post_type() {
        $labels = array(
            'name' => __( 'References', 'beotravel' ),
            'singular_name' => __( 'References', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'references',
            ),
        );
        register_post_type( 'references', $args );
    }
    add_action( 'init','references_post_type' );
}

/* FAQ post type */
if( ! function_exists( 'faq_post_type' ) ) {
    function faq_post_type() {
        $labels = array(
            'name' => __( 'FAQ', 'beogroup' ),
            'singular_name' => __( 'FAQ', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'faq',
            ),
        );
        register_post_type( 'faq', $args );
    }
    add_action( 'init','faq_post_type' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box' ) ) {
    function contact_meta_box() {
        add_meta_box(
            'contaact_box',
            'Online booking',
            'contact_meta_box_callback',
            'slider'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box' );
}
if( ! function_exists( 'contact_meta_box_callback' ) ) {
    function contact_meta_box_callback( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data', 'contact_meta_box_nonce' );
        $value = get_post_meta( $post->ID, '_contact_value_key', true );
        $url = get_post_meta( $post->ID, '_contact_url_value_key', true );
        echo '<label for="contact_label">Label: </label>';
        echo '<input type="text" id="contact_label" name="contact_label" value="' . esc_attr( $value ) . '" size="25" />';
        echo '<br>';
        echo '<label for="contact_url">Url: </label>';
        echo '<input type="text" id="contact_url" name="contact_url" value="' . esc_attr( $url ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data' ) ) {
    function contact_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce'], 'contact_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label'] ) ) {
            return;
        }
        $data1 = sanitize_text_field( $_POST['contact_label'] );
        update_post_meta( $post_id, '_contact_value_key', $data1 );
        $url1 = $_POST['contact_url'];
        update_post_meta( $post_id, '_contact_url_value_key', $url1 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data' );
}

/* Slider Meta Box */
if( ! function_exists( 'usluge_add_meta_box' ) ) {
    function usluge_add_meta_box() {
        add_meta_box(
            'usluge_box',
            'Text',
            'usluge_meta_box_callback',
            'slider'
        );
    }
    add_action( 'add_meta_boxes','usluge_add_meta_box' );
}
if( ! function_exists( 'usluge_meta_box_callback' ) ) {
    function usluge_meta_box_callback( $post ) {
        wp_nonce_field( 'usluge_meta_box_save_data','usluge_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_usluge_value_key',true );
        wp_editor( $value, 'usluge_field' );
    }
}
if( ! function_exists( 'usluge_meta_box_save_data' ) ) {
    function usluge_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['usluge_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['usluge_meta_box_nonce'],'usluge_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['usluge_field'] ) ) {
            return;
        }
        if( isset( $_POST['usluge_field'] ) ) {
            update_post_meta( $post_id,'_usluge_value_key',$_POST['usluge_field'] );
        }
    }
    add_action( 'save_post','usluge_meta_box_save_data' );
}


// Register widgets
function addWidget(){
    register_sidebar( array (
        'name' => 'Footer image',
        'id' => 'footer1',
        'before_widget' => '<div>',
		'after_widget' => '</div>',
    ));
    register_sidebar( array (
        'name' => 'Navigation',
        'id' => 'footer2',
        'before_widget' => '<div>',
		'after_widget' => '</div>',
    ));
    register_sidebar( array (
        'name' => 'Links',
        'id' => 'footer3',
        'before_widget' => '<div class="">',
		'after_widget' => '</div>',
    ));
    register_sidebar( array (
        'name' => 'Brochure',
        'id' => 'footer4',
        'before_widget' => '<div class="footer-brochure">',
		'after_widget' => '</div>',
    ));
}
add_action( 'widgets_init', 'addWidget' );

if ( ! function_exists( 'vc_before_init_actions' ) ) {
    function vc_before_init_actions() {
        require_once( get_template_directory() . '/vc-templates/slider.php' );
        require_once( get_template_directory() . '/vc-templates/services.php' );
        require_once( get_template_directory() . '/vc-templates/text.php' );
        require_once( get_template_directory() . '/vc-templates/our-team.php' );
        require_once( get_template_directory() . '/vc-templates/text-image.php' );
        require_once( get_template_directory() . '/vc-templates/references.php' );
        require_once( get_template_directory() . '/vc-templates/faq.php' );
    }
    add_action( 'vc_before_init', 'vc_before_init_actions' );
}

include __DIR__ . '/inc/social-network.php';

add_filter( 'pre_option_link_manager_enabled', '__return_true' );
