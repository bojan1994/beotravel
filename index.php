<?php

get_header();

if( have_posts() ) :
    while( have_posts() ) :
        the_post();
        the_content();
    endwhile;
    wp_reset_postdata();
else :
    _e( 'Sorry, no content found.', 'beotravel' );
endif;

get_footer();
