<?php

class vcBeotravelSlider extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_slider_mapping' ) );
        add_shortcode( 'vc_beotravel_slider', array( $this, 'vc_beotravel_slider_html' ) );
    }
    public function vc_beotravel_slider_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Slider', 'beotravel' ),
                'base' => 'vc_beotravel_slider',
                'description' => __( 'Slider', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Slider',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_slider_html( $atts ) {
        $args = array(
        	'post_type' => 'slider',
            'posts_per_page' => 10,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) : ?>
            <div id="main-carousel" class="carousel section slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $counter = 0;
                        while( $query->have_posts() ) :
                            $counter++;
                            $query->the_post(); ?>
                                <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                                    <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    <div class="carousel-caption">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_content(); ?>
                                        <?php echo nl2br(  get_post_meta( get_the_ID(), '_usluge_value_key', true ) ); ?>
                                        <?php
                                        $contact_url = get_post_meta( get_the_ID(),'_contact_url_value_key',true );
                                        $contact_label = get_post_meta( get_the_ID(),'_contact_value_key',true );
                                        if( $contact_label && $contact_url ) {
                                            ?>
                                            <a href="<?php echo $contact_url; ?>"><?php echo $contact_label; ?></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                    <?php endwhile;
                    wp_reset_postdata();
                    if ( $counter != 1 ) {
                        ?>
                        <ol class="carousel-indicators">
                        <?php
                        for( $i=0; $i<$counter; $i++ ) {
                            ?>
                            <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                            <?php
                        } ?>
                        </ol>
                        <?php
                    } ?>
                </div>
            </div>
        <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeotravelSlider();
