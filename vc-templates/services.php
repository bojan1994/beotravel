<?php

class vcBeotravelServices extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_services_mapping' ) );
        add_shortcode( 'vc_beotravel_services', array( $this, 'vc_beotravel_services_html' ) );
    }
    public function vc_beotravel_services_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services', 'beotravel' ),
                'base' => 'vc_beotravel_services',
                'description' => __( 'Services', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beotravel' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_services_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'bgcolor' => '',
                    'image' => '',
                    'title' => '',
                    'subtitle' => '',
                    'image2' => '',
                    'title2' => '',
                    'subtitle2' => '',
                    'image3' => '',
                    'title3' => '',
                    'subtitle3' => '',
                    'image4' => '',
                    'title4' => '',
                    'subtitle4' => '',
                    'image5' => '',
                    'title5' => '',
                    'subtitle5' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="">
            <div class="">
                <div class="" style="background-color:<?php echo $bgcolor; ?>">
                    <div class="">
                        <img src="<?php echo wp_get_attachment_image_src( $image, 'services' )[0]; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
                        <h3><?php echo $title; ?></h3>
                        <p><?php echo $subtitle; ?></p>
                    </div>
                    <div class="">
                        <img src="<?php echo wp_get_attachment_image_src( $image2, 'services' )[0]; ?>" alt="<?php echo $title2; ?>" title="<?php echo $title2; ?>">
                        <h3><?php echo $title2; ?></h3>
                        <p><?php echo $subtitle2; ?></p>
                    </div>
                    <div class="">
                        <img src="<?php echo wp_get_attachment_image_src( $image3, 'services' )[0]; ?>" alt="<?php echo $title3; ?>" title="<?php echo $title3; ?>">
                        <h3><?php echo $title3; ?></h3>
                        <p><?php echo $subtitle3; ?></p>
                    </div>
                    <div class="">
                        <img src="<?php echo wp_get_attachment_image_src( $image4, 'services' )[0]; ?>" alt="<?php echo $title4; ?>" title="<?php echo $title4; ?>">
                        <h3><?php echo $title4; ?></h3>
                        <p><?php echo $subtitle4; ?></p>
                    </div>
                    <div class="">
                        <img src="<?php echo wp_get_attachment_image_src( $image5, 'services' )[0]; ?>" alt="<?php echo $title5; ?>" title="<?php echo $title5; ?>">
                        <h3><?php echo $title5; ?></h3>
                        <p><?php echo $subtitle5; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeotravelServices();
