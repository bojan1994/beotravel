<?php

/**
 * Social_Network_Nav_Menu class
 *
 * @package WordPress
 *
 * @subpackage Theme_Name_With_Underscores
 *
 * @since
 *
 * @version
 *
 */

class Social_Network_Nav_Menu extends Walker_Nav_Menu

{

	public function title ( $title , $url = '' )

	{

		if ( $url === '' ) :

			return $title;

		else :

			return '<span class="sr-only">' . $title . '</span>';

		endif;

	}

	public function add_icon ( $url )

	{

		if ( $url !== '' ) :

			$scheme = parse_url ( $url , PHP_URL_SCHEME );

			$host   = parse_url ( $url , PHP_URL_HOST   );

			switch ( $scheme ) :

				case 'mailto' :

					$icon = 'fa-at';

					break;

				case 'skype' :

					$icon = 'fa-skype';

					break;

				case 'tel' :

					$icon = 'fa-phone';

					break;

				case 'whatsapp' :

					$icon = 'fa-whatsapp';

					break;

				default :

					if ( preg_match ( '/amazon.(ca|co.jp|co.uk|com|com.au|com.br|com.mx|de|fr|es|in|it|nl)/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-amazon';

					elseif ( preg_match ( '/behance.net/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-behance';

					elseif ( preg_match ( '/bitbucket.org/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-bitbucket';

					elseif ( preg_match ( '/codepen.io/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-codepen';

					elseif ( preg_match ( '/digg.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-digg';

					elseif ( preg_match ( '/dribbble.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-dribbble';

					elseif ( preg_match ( '/dropbox.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-dropbox';

					elseif ( preg_match ( '/facebook.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-facebook-f';

					elseif ( preg_match ( '/flickr.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-flickr';

					elseif ( preg_match ( '/github.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-github';

					elseif ( preg_match ( '/gitlab.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-gitlab';

					elseif ( preg_match ( '/plus.google.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-google-plus';

					elseif ( preg_match ( '/instagram.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-instagram';

					elseif ( preg_match ( '/jsfiddle.net/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-jsfiddle';

					elseif ( preg_match ( '/linkedin.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-linkedin-in';

					elseif ( preg_match ( '/pinterest.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-pinterest';

					elseif ( preg_match ( '/(imqq|qq).com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-qq';

					elseif ( preg_match ( '/reddit.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-reddit';

					elseif ( preg_match ( '/skype.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-skype';

					elseif ( preg_match ( '/slack.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-slack';

					elseif ( preg_match ( '/snapchat.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-snapchat';

					elseif ( preg_match ( '/soundcloud.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-soundcloud';

					elseif ( preg_match ( '/spotify.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-spotify';

					elseif ( preg_match ( '/stackexchange.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-stack-exchange';

					elseif ( preg_match ( '/stackoverflow.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-stack-overflow';

					elseif ( preg_match ( '/steamcommunity.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-steam';

					elseif ( preg_match ( '/tripadvisor.(at|be|ca|ch|cl|cn|co|co.hu|co.id|co.il|co.kr|co.nz|co.uk|co.za|com|com.ar|com.au|com.br|com.eg|com.gr|com.hk|com.mx|com.my|com.sg|com.pe|com.ph|com.tw|com.ve|com.vn|cz|de|dk|es|fi|fr|ie|in|it|jp|nl|pt|rs|ru|se|sk|tr)/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-tripadvisor';

					elseif ( preg_match ( '/tumblr.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-tumblr';

					elseif ( preg_match ( '/twitch.tv/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-twitch';

					elseif ( preg_match ( '/twitter.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-twitter';

					elseif ( preg_match ( '/vimeo.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-vimeo';

					elseif ( preg_match ( '/vine.co/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-vine';

					elseif ( preg_match ( '/vk.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-vk';

					elseif ( preg_match ( '/whatsapp.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-whatsapp';

					elseif ( preg_match ( '/wordpress.(com|org)/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-wordpress';

					elseif ( preg_match ( '/youtube.com/' , $host , $matches , 0 , 0 ) ) :

						$icon = 'fa-youtube';

					else :

						$icon = 'fa-link';

					endif;

			endswitch;

			$output = sprintf ( '<i class="fab %1$s" aria-hidden="true"></i>' , $icon );

		else :

			$output = '';

		endif;

		return $output;

	}

	public function start_lvl ( &$output , $depth = 0 , $args = array () )

	{

		if ( isset ( $args -> item_spacing ) and 'discard' === $args -> item_spacing ) :

			$t = '';

			$n = '';

		else :

			$t = "\t";

			$n = "\n";

		endif;

		$indent = str_repeat ( $t , $depth );

		$classes = array ( 'sub-menu' );

		$class_names = join ( ' ' , apply_filters ( 'nav_menu_submenu_css_class' , $classes , $args , $depth ) );

		$class_names = $class_names ? ' class="' . esc_attr ( $class_names ) . '"' : '';

		$output .= "{$n}{$indent}<ul $class_names>{$n}";

	}

	public function end_lvl ( &$output , $depth = 0 , $args = array () )

	{

		if ( isset ( $args -> item_spacing ) and 'discard' === $args -> item_spacing ) :

			$t = '';

			$n = '';

		else :

			$t = "\t";

			$n = "\n";

		endif;

		$indent = str_repeat ( $t , $depth );

		$output .= "$indent</ul>{$n}";

	}

	public function start_el ( &$output , $item , $depth = 0 , $args = array () , $id = 0 )

	{

		if ( isset ( $args -> item_spacing ) and 'discard' === $args -> item_spacing ) :

			$t = '';

			$n = '';

		else :

			$t = "\t";

			$n = "\n";

		endif;

		$indent = ( $depth ) ? str_repeat ( $t , $depth ) : '';

		$classes = empty ( $item -> classes ) ? array () : (array) $item -> classes;

		$classes [] = 'menu-item-' . $item -> ID;

		$args = apply_filters ( 'nav_menu_item_args' , $args , $item , $depth );

		$class_names = join ( ' ' , apply_filters ( 'nav_menu_css_class' , array_filter ( $classes , 'strval' , 0 ) , $item , $args , $depth ) );

		$class_names = $class_names ? ' class="' . esc_attr ( $class_names ) . '"' : '';

		$id = apply_filters ( 'nav_menu_item_id' , 'menu-item-'. $item -> ID , $item , $args , $depth );

		$id = $id ? ' id="' . esc_attr ( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $class_names . '>';

		$atts = array ();

		$atts [ 'title' ]  = ! empty ( $item -> attr_title ) ? $item -> attr_title : '';

		$atts [ 'target' ] = ! empty ( $item -> target )     ? $item -> target     : '';

		$atts [ 'rel' ]    = ! empty ( $item -> xfn )        ? $item -> xfn        : '';

		$atts [ 'href' ]   = ! empty ( $item -> url )        ? $item -> url        : '';

		$atts = apply_filters ( 'nav_menu_link_attributes' , $atts , $item , $args , $depth );

		$attributes = '';

		foreach ( $atts as $attr => $value ) :

			if ( ! empty ( $value ) ) :

				$value = ( 'href' === $attr ) ? esc_url ( $value ) : esc_attr ( $value );

				$attributes .= ' ' . $attr . '="' . $value . '"';

			endif;

		endforeach;

		$title = apply_filters ( 'the_title' , $item -> title , $item -> ID , $depth );

		$title = apply_filters ( 'nav_menu_item_title' , $title , $item , $args , $depth );

		$item_output = $args -> before;

		$item_output .= '<a' . $attributes . '>';

		$item_output .= $args -> link_before;

		$item_output .= $this -> add_icon ( $item -> url );

		$item_output .= $this -> title    ( $title , $item -> url );

		$item_output .= $args -> link_after;

		$item_output .= '</a>';

		$item_output .= $args -> after;

		$output .= apply_filters ( 'walker_nav_menu_start_el' , $item_output , $item , $depth , $args );

	}

	public function end_el ( &$output , $item , $depth = 0 , $args = array () )

	{

		if ( isset ( $args -> item_spacing ) and 'discard' === $args -> item_spacing ) :

			$t = '';

			$n = '';

		else :

			$t = "\t";

			$n = "\n";

		endif;

		$output .= "</li>{$n}";

	}

} // Social_Network_Nav_Menu
