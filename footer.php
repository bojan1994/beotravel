<footer class="site-footer">
    <div class="container footer-container">
        <div class="footer-logo">
            <?php
            if( is_active_sidebar( 'footer1' )) {
                dynamic_sidebar( 'footer1' );
            }
            ?>
        </div>
        <div class="footer-nav">
            <?php
            if( is_active_sidebar( 'footer2' )) {
                dynamic_sidebar( 'footer2' );
            }
            ?>
        </div>
        <div class="footer-links clearfix">
            <?php
            if( is_active_sidebar( 'footer3' )) {
                dynamic_sidebar( 'footer3' );
            }
            ?>
        </div>
        <div class="footer-search clearfix">
            <div class="tnp tnp-subscription">
                <h4>Newsletter</h4>
                <form method="post" action="http://beotravel.test/?na=s" onsubmit="return newsletter_check(this)">
                    <input type="hidden" name="nlang" value="">
                    <div class="tnp-field tnp-field-email"><input class="tnp-email" type="email" name="ne" placeholder="Your E-mail" required></div>
                    <div class="tnp-field tnp-field-button"><input class="tnp-submit" type="submit" value="Subscribe"></div>
                </form>
            </div>
            <?php
            if( is_active_sidebar( 'footer4' )) {
                dynamic_sidebar( 'footer4' );
            }
            ?>
            <div class="footer-find">
                <p>Find us on</p>
                <?php
                $args = array(
                    'theme_location' => 'social',
                    'walker' => new Social_Network_Nav_Menu(),
                );
                wp_nav_menu( $args );
                ?>
            </div>
        </div>

    </div>
    <div class="footer-bottom">
        <div class="footer-bottom-container container">
            <p class="footer-paragraph"><?php echo nl2br( get_theme_mod( 'custom_footer_text' ) ); ?></p>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>

</html>
